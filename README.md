﻿

# Novo Challenge
## Preface

Le travail m'a pris environ 5 journées de 8h (non contigus). J'ai préféré soumettre quelque chose de fonctionnel pour démontrer mon potentiel plutôt que ma rapidité. Aussi, le projet était vraiment amusant à développer, je désirais vraiment continuer. 

Après 8h, j'avais un backend fonctionnel, et l'affichage simple des données mise à jour. Le reste du temps a été utilisé pour expérimenter des désigns visuels, paufiner le "responsive", optimiser les composants, la gestion d'état, l'environnement virtualisé et la documentation.

###  Backend
J'ai choisi d'utiliser l'écosystème NGINX/PHP/Mariadb, avec Laravel 5.7 comme framework.

Avantages :

- Laravel est très puissant, il possède beaucoup de fonctionalités pour faire à peu près nimporte quel type de backend rapidement, comme par exemple un RESTful API. 
- Il est livré avec un ORM très facile d'utilisation avec très peu de code.
- L'utilitaire de ligne de commande *artisan* nous permet de générer des controlleurs RESTful, modèles ORM, migrations, seeders etc.
- L'écosystème PHP ne "coute pas cher" à héberger (hébergement traditionel)
- L'utilisation d'une base de donnée relationelle permet d'appliquer les principes [ACID](https://en.wikipedia.org/wiki/ACID_(computer_science)  . Cela peut être souhaitable dans le cas de la sauvegarde de données médicales critiques.
- J'ai déja une certaine expérience dans Laravel, ce qui m'a permis d'allouer plus de temps à l'expérience utilisateur du frontend.

Désavantages :

- L'écosystème PHP est lourd, et nécéssite le fonctionnement à l'unisson de plusieurs composants, ce qui nuit grandement à la facilité de déploiement. Plusieurs solutions peuvent aider à contourner ce problème comme l'utilisation d'environnements virtualisés et de conteneurisation. Le développement et le test de tels outils prends du temps. J'ai malheureusement dû utiliser beaucoup de temps pour configurer un environnement virtualisé me permettant de vous soumettre mon projet.
- L'écosystème PHP a fait ses preuves mais avec l'utilisation toujours plus fréquente des services cloud comme AWS ou Azure, une solution basée sur NodeJs ou .net seraient définitivement de meilleurs choix pour le déploiement et la performance.

###  Frontend
J'ai choisi d'utiliser Angular2+ pour la partie client. J'ai commencé à l'apprendre après le temp des fêtes et je suis tombé en amour avec TypeScript et le framework Angular. Le framework est bâti sur des pattern solides comme la gestion des traitements asyncrones avec RxJs, l'utilisation de mécanismes d'injection de dépendances, les Schematics pour accélérer la création de "Boilerplate code", etc.

Plusieurs librairies existent pour nous faciliter la vie, accélérer la création d'interface utilisateur, et augmenter le découplage entre les couches de notre application. Le projet utilise entre autres les librairies suivantes :

 - Ngrx
 - angular/material
 - angular/flex-layout
 - ng2-charts (chartJs)
 - rxjs

Je me suis dernièrement intéressé au pattern "state container", implémenté entre autres par Redux (React), Vuex (VueJs) et Ngrx (Angular). Cela permet d'isoler les "side effects" de l'application et avoir une seule source de vérité. Le projet utilise Ngrx pour gerer les états de l'application et les appels d'API pour les patients, ses signes vitaux ainsi que ses statistiques.

## Implementation
###  Frontend

Fonctionalités :

- Ajouter/Supprimer/Modifier patient.
- Affichage des informations du patient.
- Affichage des signes vitaux mises à jours toute les 3 secondes. 
- Affichage des statistiques mises à jour toute les minutes.

J'ai décidé d'utiliser une statégie de "Polling" pour mettre à jour les signes vitaux car c'est plus simple que d'utiliser un "message broker" ou autres stratégies "realtime".

Je me suis inspiré de Material Design pour concevoir l'interface.

#### Live demo :

Frontend : [http://novo-challenge.duckdns.org](http://novo-challenge.duckdns.org)

Backend : [http://novo-backend.duckdns.org](http://novo-backend.duckdns.org)

Demo video :

[![NovoChallengeVideoPresentation](http://img.youtube.com/vi/50mL5w9uPrY/0.jpg)](http://www.youtube.com/watch?v=50mL5w9uPrY)

Vue d'ensemble :

![Overview Diagram](https://bitbucket.org/barsenault/novo-challenge/raw/1f1e6c18665a56091988956b418aebc92a2e68c9/doc/diagram.png "Overview diagram")


###  Backend



API endpoints :

- Données patient :
 
    [GET api/patient](http://novo-backend.duckdns.org/api/patient)

- Archive de signes vitaux. Chaque minute, un "snapshot" des derniers signes vitaux est ajouté. Le Scheduler de Laravel (déclanché par cron) a été utilisé pour générer le "snapshot"

    [GET api/vitalsign](http://novo-backend.duckdns.org/api/vitalsign)

- Retourne les plus récents signes vitaux, il sont générés aléatoirement à chaque requète.

    [GET api/vitalsign/latest](http://novo-backend.duckdns.org/api/vitalsign/latest)
        
- Retourne les statistiques de signes vitaux pour tous les patient à l'aide des fonctions d'aggrégation SQL .

    [GET api/vitalsign/stats](http://novo-backend.duckdns.org/api/vitalsign/stats)

- Retourne les statistiques de signes vitaux pour un seul patient

    [GET api/vitalsign/stats/{patient_id}](http://novo-backend.duckdns.org/api/vitalsign/stats/1)

## Utilisation du projet
### Backend
#### Introduction
Pour les raisons mentionnées dans le préface, j'ai du utiliser Vagrant et Docker pour pouvoir vous soumettre mon travail. J'ai utilisé Laradock pour fournir les containers requis pour mon application. Cette solution fonctionne sous linux sans nécéssiter de machine virtuelle car linux peut éxecuter Docker nativement. Pour windows, j'ai enrobé ma solution Docker avec une machine virtuelle Vagrant.

##### [Laradock](https://laradock.io/) :

> A full PHP development environment for Docker. Includes pre-packaged
> Docker Images, all pre-configured to provide a wonderful PHP
> development environment. Laradock is well known in the Laravel
> community, as the project started with single focus on running Laravel
> projects on Docker. Later and due to the large adoption from the PHP
> community, it started supporting other PHP projects like Symfony,
> CodeIgniter, WordPress, Drupal…

##### [Vagrant](https://www.vagrantup.com/) :

> Vagrant is a tool for building and managing virtual machine
> environments in a single workflow. With an easy-to-use workflow and
> focus on automation, Vagrant lowers development environment setup
> time, increases production parity, and makes the "works on my machine"
> excuse a relic of the past.

#### Backend
##### Windows instructions (recommended)  :
- Install VirtualBox
- Install Vagrant
- Clone the project

		 git clone https://bitbucket.org/barsenault/novo-challenge.git

- Start vagrant box (first build can take a while, please be patient)		
	   
	    cd novo-challenge/backend
	    vagrant up
- Once completed, open browser at http://localhost:4300
		
##### Linux instructions (experimental) :
- Install Docker
- Install docker-compose
- Clone the project

		 git clone https://bitbucket.org/barsenault/novo-challenge.git
-   sudo privilege is needed to execute docker commands. You should add your user to the docker group to avoid this situation.
			
			usermod -aG docker $USER 
			
- Re-open your console to reload permissions
- Start the backend	

	    cd novo-challenge/backend/laradock
	    ./novo-build.sh
	    
- Open browser at http://localhost:4300
	
	Note: If you encounter an error, try running the script with sudo (after inspecting it to ensure it is safe to run on your system).
	
### Frontend
#### Angular 7.2
#### How to use
 - Install NodeJs
 - Clone the project :
 
		git clone git@bitbucket.org/novo-challenge.git novo-challenge
	
- Install Angular cli

		npm install -g @angular/cli

- Serve the application :
 
		cd novo-challenge/frontend
		ng serve
	
- Open your browser at http://localhost:4200
