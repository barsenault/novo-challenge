<?php

use Faker\Generator as Faker;

$factory->define(App\Patient::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname' => $faker->lastName,
        'email' => $faker->safeEmail,
        'age' => $faker->numberBetween(1,125),
        'sex' => $faker->randomElement(['male', 'female'])
    ];
});
