<?php

namespace App\Console;

use App\Patient;
use App\VitalSign;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //Record a VitalSign for each patient every minute
        $schedule->call(function () {
            Patient::all()->each(function (Patient $patient) {
                $patient->vitalSign()->save(VitalSign::getRandomVitalSign($patient['id']));
            });
        })->everyMinute();

        //Prune stats over 10 minutes old to avoid filling the db
        $schedule->call(function () {
            VitalSign::all()
                ->where('created_at', '<', Carbon::now()->subMinutes(10))
                ->each(function ($stat) {
                    $stat->delete();
                });
        })->everyTenMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
