<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VitalSign extends Model
{
    protected $fillable = ['id','patient_id','freq', 'temp', 'spo2'];

    public static function getRandomVitalSign($patientId){
        $mult = 100;
        return new VitalSign([
                'patient_id' => $patientId,
                'freq' => mt_rand(60 * $mult, 140 * $mult) / $mult,
                'temp' => mt_rand(35 * $mult, 45 * $mult) / $mult,
                'spo2' => mt_rand(95 * $mult, 100 * $mult) / $mult
            ]);
    }
}
