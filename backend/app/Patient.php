<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'age',
        'sex'
    ];

    public function vitalSign(){
        return $this->hasMany(VitalSign::class);
    }
}
