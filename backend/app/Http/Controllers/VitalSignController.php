<?php

namespace App\Http\Controllers;

use App\Patient;
use App\VitalSign;
use Illuminate\Http\Request;

class VitalSignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return VitalSign::all();
    }

    public function latest(){
        //Randomly generate fake VitalSign for each patient
        $vitalSigns = [];
        Patient::all()->each(function(Patient $item, $key) use (&$vitalSigns){
            array_push($vitalSigns,VitalSign::getRandomVitalSign($item['id']));
        });
        return response()->json($vitalSigns);
    }

    public function stats(Patient $patient){
        return response()->json($this->aggregatePatientStat($patient));
    }

    public function allStats(Patient $patient){
        $stats = [];
        Patient::all()->each(function(Patient $patient, $key) use (&$stats){
            array_push($stats, $this->aggregatePatientStat($patient));
        });
        return response()->json($stats);

    }

    private function aggregatePatientStat(Patient $patient){
        return [
            'patient_id' => $patient['id'],
            'freq' => [
                'min' => $patient->vitalSign()->min('freq'),
                'max' => $patient->vitalSign()->max('freq'),
                'avg' => $patient->vitalSign()->avg('freq'),
            ],
            'temp' => [
                'min' => $patient->vitalSign()->min('temp'),
                'max' => $patient->vitalSign()->max('temp'),
                'avg' => $patient->vitalSign()->avg('temp')
            ],
            'spo2' => [
                'min' => $patient->vitalSign()->min('spo2'),
                'max' => $patient->vitalSign()->max('spo2'),
                'avg' => $patient->vitalSign()->avg('spo2')
            ]
        ];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\VitalSign  $vitalSign
     * @return \Illuminate\Http\Response
     */
    public function show(VitalSign $vitalSign)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\VitalSign  $vitalSign
     * @return \Illuminate\Http\Response
     */
    public function edit(VitalSign $vitalSign)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\VitalSign  $vitalSign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, VitalSign $vitalSign)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\VitalSign  $vitalSign
     * @return \Illuminate\Http\Response
     */
    public function destroy(VitalSign $vitalSign)
    {
        //
    }
}
