#!/usr/bin/env bash

docker-compose down
echo Starting Laravel required containers...
docker-compose up -d --build nginx mariadb

echo Pulling composer packages...
docker-compose exec -T --user=laradock workspace composer install

echo Migrating/Seeding Laravel database...
docker-compose exec -T --user=laradock workspace php artisan migrate:fresh --seed
echo ----------------------------------------------------
echo NovoChallenge backend has been rebuilt and re-seeded
echo ----------------------------------------------------
echo Backend boot complete !
echo open browser at http://localhost
