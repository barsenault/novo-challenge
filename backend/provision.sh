#!/usr/bin/env bash

#sudo apt-get install apt-transport-dockerhttps ca-certificates curl software-properties-common
#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
#sudo apt-get update
#sudo apt-get install docker-ce -y
#sudo apt-get install python-pip -y
#sudo pip install docker-compose
#sudo usermod -aG docker vagrant
cd /vagrant/laradock
sudo usermod -aG docker vagrant
echo Starting Laravel required containers...
sudo docker-compose up -d --build nginx mariadb

echo Pulling composer packages...
sudo docker-compose exec -T --user=laradock workspace composer install

echo Migrating/Seeding Laravel database...
sudo docker-compose exec -T --user=laradock workspace php artisan migrate --seed

sudo docker-compose down
