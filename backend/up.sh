#!/usr/bin/env bash
cd /vagrant/laradock
echo Starting nginx and mariadb...
sudo docker-compose up -d nginx mariadb
echo ----------------------------------------------------
echo Backend boot complete !
echo open browser at http://localhost:4300
echo ----------------------------------------------------
echo You can log to your vagrant box by typing : 
echo vagrant ssh
echo ----------------------------------------------------
echo Once you\'re inside the vagrant box, you can access the php workspace docker container by typing : 
echo cd /vagrant/laradock
echo docker-compose exec --user=laradock workspace bash
echo ----------------------------------------------------
