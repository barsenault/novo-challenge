<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('patient', 'PatientController');
Route::get('vitalsign', 'VitalSignController@index');
Route::get('vitalsign/latest', 'VitalSignController@latest');
Route::get('vitalsign/stats', 'VitalSignController@allstats');
Route::get('vitalsign/stats/{patient}', 'VitalSignController@stats');
//Route::apiResource('vitalsign', 'VitalSignController');

