import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NavigationComponent} from './navigation/navigation.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MaterialModule} from '../material/material.module';
import {RouterModule} from '@angular/router';
import {PatientSummaryModule} from '../patient-summary/patient-summary.module';

@NgModule({
  declarations: [NavigationComponent, DashboardComponent],
  imports: [
    CommonModule,
      FlexLayoutModule,
      MaterialModule,
      RouterModule,
      PatientSummaryModule,
  ],
  exports: [NavigationComponent]
})
export class AppLayoutModule { }
