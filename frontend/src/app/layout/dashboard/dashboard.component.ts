import {Component, OnInit} from '@angular/core';
import {Store} from '@ngrx/store';
import {selectAllVitalSignStats, selectVitalSignByPatientId} from '../../vitalsign/store/vital-sign-stat.reducer';
import {PollVitalSignStats} from '../../vitalsign/store/vital-sign-stat.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {


  constructor(private store: Store<any>) { }

  ngOnInit(): void {

  }
}
