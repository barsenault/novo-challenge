import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor(private snackbar: MatSnackBar) {
    }

    error(message: string) {
        this.snackbar.open(message, 'Dismiss', {duration: 2500});
    }

    info(message: string) {
        this.snackbar.open(message, 'Dismiss', {duration: 2500});
    }
}
