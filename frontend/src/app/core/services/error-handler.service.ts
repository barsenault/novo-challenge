import {ErrorHandler, Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {NotificationService} from './notification.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService extends ErrorHandler {

  constructor(private notificationService: NotificationService) {
    super();
  }

  handleError(error: Error | HttpErrorResponse): void {
    const message = 'An error occurred. See console for details';
    this.notificationService.error(message);
    console.error(error.message);
    super.handleError(error);
  }
}
