import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActionReducerMap, StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';

import * as vitalSignReducer from './store/vital-sign.reducer';
import * as vitalSignStatReducer from './store/vital-sign-stat.reducer';
import {VitalSignEffects} from './store/vital-sign.effects';
import {VitalSignComponent} from './components/vital-sign/vital-sign.component';
import {MaterialModule} from '../material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {VitalSignStatEffects} from './store/vital-sign-stat.effects';
import * as fromRoot from '../reducers';
import { VitalSignStatComponent } from './components/vital-sign-stat/vital-sign-stat.component';
import {ChartsModule} from 'ng2-charts';

@NgModule({
    declarations: [VitalSignComponent, VitalSignStatComponent],
    imports: [
        CommonModule,
        StoreModule.forFeature('vitalsign', vitalSignReducer.reducer),
        StoreModule.forFeature('vitalsignstat', vitalSignStatReducer.reducer),
        EffectsModule.forFeature([VitalSignEffects, VitalSignStatEffects]),
        MaterialModule,
        FlexLayoutModule,
        ChartsModule
    ],
    exports: [VitalSignComponent, VitalSignStatComponent]
})
export class VitalSignModule {
}
