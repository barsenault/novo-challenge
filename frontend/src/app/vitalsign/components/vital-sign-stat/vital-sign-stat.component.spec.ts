import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VitalSignStatComponent } from './vital-sign-stat.component';

describe('VitalSignStatComponent', () => {
  let component: VitalSignStatComponent;
  let fixture: ComponentFixture<VitalSignStatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VitalSignStatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VitalSignStatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
