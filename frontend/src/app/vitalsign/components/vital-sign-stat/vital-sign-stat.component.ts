import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-vital-sign-stat',
  templateUrl: './vital-sign-stat.component.html',
  styleUrls: ['./vital-sign-stat.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VitalSignStatComponent implements OnInit {

  @Input() vitalsignstat;
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['temp', 'freq', 'spo2'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData:any[] = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  ];

  constructor() { }

  ngOnInit() {
  }

}
