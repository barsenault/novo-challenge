import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {VitalSign} from '../../store/vital-sign.model';


@Component({
  selector: 'app-vital-sign',
  templateUrl: './vital-sign.component.html',
  styleUrls: ['./vital-sign.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class VitalSignComponent implements OnInit {

  @Input() vitalsign: VitalSign;
  constructor() { }

  ngOnInit() {
  }

}
