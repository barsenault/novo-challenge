import {Action} from '@ngrx/store';
import {Update} from '@ngrx/entity';
import {VitalSignStat} from './vital-sign-stat.model';

export enum VitalSignStatActionTypes {
    PollVitalSignStats = '[VitalSignStat] Poll VitalSignStats',
    PollVitalSignStatsSuccess = '[VitalSignStat] Poll VitalSignStats Success',
    StopVitalSignStatsPolling = '[VitalSignStat] Stop VitalSignStats Polling'
}

export class PollVitalSignStats implements Action {
    readonly type = VitalSignStatActionTypes.PollVitalSignStats;
}

export class PollVitalSignStatsSuccess implements Action {
    readonly type = VitalSignStatActionTypes.PollVitalSignStatsSuccess;

    constructor(public payload: { vitalSignStats: VitalSignStat[] }) {
    }
}

export class StopVitalSignStatsPolling implements Action {
    readonly type = VitalSignStatActionTypes.StopVitalSignStatsPolling;
}

export type VitalSignStatActions =
    PollVitalSignStats
    | PollVitalSignStatsSuccess
    | StopVitalSignStatsPolling;
