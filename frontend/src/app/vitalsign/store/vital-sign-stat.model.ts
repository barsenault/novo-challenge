export interface VitalSignStat {
  id: string;
  patient_id: string;
  freq: VitalSignStatItem;
  temp: VitalSignStatItem;
  spo2: VitalSignStatItem;
}

export interface VitalSignStatItem {
  min: number;
  max: number;
  avg: number;
}
