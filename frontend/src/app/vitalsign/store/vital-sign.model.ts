export interface VitalSign {
  id: string;
  patient_id: string;
  freq: number;
  temp: number;
  spo2: number;
}
