import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { VitalSignEffects } from './vital-sign.effects';

describe('VitalSignEffects', () => {
  let actions$: Observable<any>;
  let effects: VitalSignEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        VitalSignEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(VitalSignEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
