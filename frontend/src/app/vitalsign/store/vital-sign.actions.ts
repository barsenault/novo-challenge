import {Action} from '@ngrx/store';
import {VitalSign} from './vital-sign.model';
import {HttpErrorResponse} from '@angular/common/http';

export enum VitalSignActionTypes {
    PollVitalSigns = '[VitalSign] Poll VitalSigns',
    PollVitalSignsSuccess = '[VitalSignService] Poll VitalSigns Success',
    StopVitalSignsPolling = '[VitalSign] Stop VitalSigns Polling',
    VitalSignApiError = '[VitalSignService] Api error'
}

export class PollVitalSigns implements Action {
    readonly type = VitalSignActionTypes.PollVitalSigns;
}

export class StopVitalSignsPolling implements Action {
    readonly type = VitalSignActionTypes.StopVitalSignsPolling;
}

export class PollVitalSignsSuccess implements Action {
    readonly type = VitalSignActionTypes.PollVitalSignsSuccess;

    constructor(public payload: { vitalSigns: VitalSign[] }) {
    }
}

export class VitalSignApiError implements Action {
    readonly type = VitalSignActionTypes.VitalSignApiError;

    constructor(public payload: { error: HttpErrorResponse }) {
    }
}

export type VitalSignActions =
    | PollVitalSigns
    | StopVitalSignsPolling
    | PollVitalSignsSuccess
    | VitalSignApiError;
