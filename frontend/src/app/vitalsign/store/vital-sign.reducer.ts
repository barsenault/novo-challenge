import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {VitalSign} from './vital-sign.model';
import {VitalSignActions, VitalSignActionTypes} from './vital-sign.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export interface State extends EntityState<VitalSign> {
    // additional entities state properties
    polling: boolean;
}

export const adapter: EntityAdapter<VitalSign> = createEntityAdapter<VitalSign>();

export const initialState: State = adapter.getInitialState({
    // additional entity state properties
    polling: false
});

export function reducer(
    state = initialState,
    action: VitalSignActions
): State {
    switch (action.type) {
        case VitalSignActionTypes.PollVitalSigns: {
            return {...state, polling: true};
        }
        case VitalSignActionTypes.StopVitalSignsPolling: {
            return {...state, polling: false};
        }
        case VitalSignActionTypes.PollVitalSignsSuccess: {
            return adapter.upsertMany(action.payload.vitalSigns, state);
        }

        default: {
            return state;
        }
    }
}

export const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();

export const getPollState = (state: State) => state.polling;

export const selectVitalSignState = createFeatureSelector<State>('vitalsign');
export const selectAllVitalSigns = createSelector(selectVitalSignState, selectAll);
export const selectPollState = createSelector(selectVitalSignState, getPollState);
