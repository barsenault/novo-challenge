import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {PollVitalSignsSuccess, VitalSignActionTypes, VitalSignApiError} from './vital-sign.actions';
import {catchError, filter, map, switchMap, takeUntil, tap} from 'rxjs/operators';
import {of, timer} from 'rxjs';
import {VitalSign} from './vital-sign.model';
import {VitalSignService} from '../services/vital-sign.service';
import {ErrorHandlerService} from '../../core/services/error-handler.service';
import {Store} from '@ngrx/store';
import {selectPollState} from './vital-sign.reducer';

@Injectable()
export class VitalSignEffects {

    @Effect()
    startPolling$ = this.actions$.pipe(
        ofType(VitalSignActionTypes.PollVitalSigns),
        switchMap(() =>
            timer(0, 10000).pipe(
                takeUntil(this.store$.select(selectPollState).pipe(
                    filter(isPolling => isPolling === false)
                )),
                switchMap(() => this.vitalSignService.getVitalSigns().pipe(
                    map((vitalSigns: VitalSign[]) => {
                        return new PollVitalSignsSuccess({
                            vitalSigns: vitalSigns.map((value: VitalSign) => ({...value, id: value.patient_id}))
                        });
                    }),
                    catchError(err => of(new VitalSignApiError(err)))
                ))
            )
        )
    );

    @Effect({dispatch: false})
    vitalSignApiError$ = this.actions$.pipe(
        ofType(VitalSignActionTypes.VitalSignApiError),
        tap(error => {
            this.errorHandler.handleError(error);
        })
    );

    constructor(private actions$: Actions,
                private store$: Store<any>,
                private vitalSignService: VitalSignService,
                private errorHandler: ErrorHandlerService) {
    }
}
