import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {VitalSignApiError} from './vital-sign.actions';
import {catchError, filter, map, switchMap, takeUntil} from 'rxjs/operators';
import {of, timer} from 'rxjs';
import {VitalSignService} from '../services/vital-sign.service';
import {PollVitalSignStatsSuccess, VitalSignStatActionTypes} from './vital-sign-stat.actions';
import {VitalSignStat} from './vital-sign-stat.model';
import {Store, Action} from '@ngrx/store';
import {selectPollState} from './vital-sign-stat.reducer';

@Injectable()
export class VitalSignStatEffects {

    @Effect()
    loadVitalSignStats$ = this.actions$.pipe(
        ofType(VitalSignStatActionTypes.PollVitalSignStats),
        switchMap(() =>
            timer(0, 30000).pipe(
                takeUntil(this.store$.select(selectPollState).pipe(
                    filter(isPolling => isPolling === false))),
                switchMap(() => this.vitalSignService.getVitalSignsStats().pipe(
                    map((vitalSignsStats: VitalSignStat[]) => {
                        return new PollVitalSignStatsSuccess({
                            vitalSignStats: vitalSignsStats.map((value: VitalSignStat) => ({...value, id: value.patient_id}))
                        });
                    }),
                    catchError(err => of(new VitalSignApiError(err)))
                ))
            )
        )
    );

    constructor(private actions$: Actions,
                private store$: Store<any>,
                private vitalSignService: VitalSignService) {
    }
}
