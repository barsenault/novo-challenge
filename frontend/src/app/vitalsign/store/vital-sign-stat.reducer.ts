import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {VitalSignStat} from './vital-sign-stat.model';
import {VitalSignStatActions, VitalSignStatActionTypes} from './vital-sign-stat.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export interface State extends EntityState<VitalSignStat> {
    // additional entities state properties
    polling: boolean;
}

export const adapter: EntityAdapter<VitalSignStat> = createEntityAdapter<VitalSignStat>();

export const initialState: State = adapter.getInitialState({
    // additional entity state properties
    polling: false
});

export function reducer(
    state = initialState,
    action: VitalSignStatActions
): State {
    switch (action.type) {
        case VitalSignStatActionTypes.PollVitalSignStats: {
            return {...state, polling: true};
        }
        case VitalSignStatActionTypes.PollVitalSignStatsSuccess: {
            return adapter.addAll(action.payload.vitalSignStats, state);
        }
        case VitalSignStatActionTypes.StopVitalSignStatsPolling: {
            return {...state, polling: false};
        }

        default: {
            return state;
        }
    }
}

export const {
    selectIds,
    selectEntities,
    selectAll,
    selectTotal,
} = adapter.getSelectors();
export const getPollState = (state: State) => state.polling;

export const selectVitalSignStatsState = createFeatureSelector<State>('vitalsignstat');
export const selectAllVitalSignStats = createSelector(selectVitalSignStatsState, selectAll);
export const selectVitalSignByPatientId = createSelector(selectAllVitalSignStats, (allStats, props) => {
    return allStats.find(item => item.id === props.id);
});

export const selectPollState = createSelector(selectVitalSignStatsState, getPollState);
