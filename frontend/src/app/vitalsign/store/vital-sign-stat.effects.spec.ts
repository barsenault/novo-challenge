import { TestBed, inject } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';

import { VitalSignStatEffects } from './vital-sign-stat.effects';

describe('VitalSignStatEffects', () => {
  let actions$: Observable<any>;
  let effects: VitalSignStatEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        VitalSignStatEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(VitalSignStatEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
