import { TestBed } from '@angular/core/testing';

import { VitalSignService } from './vital-sign.service';

describe('VitalSignService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VitalSignService = TestBed.get(VitalSignService);
    expect(service).toBeTruthy();
  });
});
