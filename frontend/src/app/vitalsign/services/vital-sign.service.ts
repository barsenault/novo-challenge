import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {VitalSign} from '../store/vital-sign.model';
import {HttpClient} from '@angular/common/http';
import {VitalSignStat} from '../store/vital-sign-stat.model';

// Todo: extract extract endpoint host into a settings service
export const ENDPOINT = 'http://novo-backend.duckdns.org/api/vitalsign/';

@Injectable({
    providedIn: 'root'
})
export class VitalSignService {

    constructor(private http: HttpClient) {
    }

    getVitalSigns(): Observable<VitalSign[]> {
        return this.http.get<VitalSign[]>(ENDPOINT + 'latest');
    }

    getVitalSignsStats(): Observable<VitalSignStat[]> {
        return this.http.get<VitalSignStat[]>(ENDPOINT + 'stats');
    }
}
