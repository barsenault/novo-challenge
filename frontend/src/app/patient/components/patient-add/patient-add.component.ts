import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {PatientFormService} from '../../services/patient-form.service';

@Component({
    selector: 'app-patient-add',
    templateUrl: './patient-add.component.html',
    styleUrls: ['./patient-add.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PatientAddComponent implements OnInit {

    constructor(public formService: PatientFormService) {
        formService.reset();
    }

    ngOnInit() {

    }


}
