import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {PatientFormService} from '../../services/patient-form.service';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-patient-edit',
  templateUrl: './patient-edit.component.html',
  styleUrls: ['./patient-edit.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PatientEditComponent implements OnInit {

  constructor(public formService: PatientFormService,  @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.formService.formGroup.setValue(this.data, {});
  }

}
