import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {PatientFormService} from '../../services/patient-form.service';



@Component({
    selector: 'app-patient-form',
    templateUrl: './patient-form.component.html',
    styleUrls: ['./patient-form.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PatientFormComponent implements OnInit {


  formGroup = this.patientFormService.formGroup;
  genderOptions = PatientFormService.GENDER_OPTIONS;

  constructor(public patientFormService: PatientFormService) { }

  ngOnInit() {

  }

}
