import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Patient} from '../../store/patient.model';

@Component({
    selector: 'app-patient-info',
    templateUrl: './patient-info.component.html',
    styleUrls: ['./patient-info.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PatientInfoComponent implements OnInit {

    @Input() patient: Patient;

    constructor() {
    }

    ngOnInit() {
    }

    get fullname() {
        return this.patient.firstname + ' ' + this.patient.lastname;
    }

    get email() {
        return this.patient.email;
    }

    get age() {
        return this.patient.age;
    }

    get sex() {
        return this.patient.sex;
    }
}
