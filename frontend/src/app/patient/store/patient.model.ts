export interface Patient {
  id?: string;
  firstname: string;
  lastname: string;
  email: string;
  age: number;
  sex: string;
}

