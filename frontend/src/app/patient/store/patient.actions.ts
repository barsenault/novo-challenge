import {Action} from '@ngrx/store';
import {Update} from '@ngrx/entity';
import {Patient} from './patient.model';
import {HttpErrorResponse} from '@angular/common/http';

export enum PatientActionTypes {
    LoadPatients = '[Patient] Load Patients',
    LoadPatientsSuccess = '[PatientService] Load Patients Success',
    AddPatient = '[Patient] Add Patient',
    AddPatientSuccess = '[PatientService] Add Patient Success',
    UpdatePatient = '[Patient] Update Patient',
    UpdatePatientSuccess = '[PatientService] Update Patient Success',
    DeletePatient = '[Patient] Delete Patient',
    DeletePatientSuccess = '[PatientService] Delete Patient Success',
    PatientApiError = '[PatientService] API Error'
}

export class LoadPatients implements Action {
    readonly type = PatientActionTypes.LoadPatients;
}

export class LoadPatientsSuccess implements Action {
    readonly type = PatientActionTypes.LoadPatientsSuccess;

    constructor(public payload: { patients: Patient[] }) {
    }
}

export class AddPatient implements Action {
    readonly type = PatientActionTypes.AddPatient;

    constructor(public payload: { patient: Patient }) {
    }
}

export class AddPatientSuccess implements Action {
    readonly type = PatientActionTypes.AddPatientSuccess;

    constructor(public payload: { patient: Patient }) {
    }
}

export class UpdatePatient implements Action {
    readonly type = PatientActionTypes.UpdatePatient;

    constructor(public payload: { patient: Update<Patient> }) {
    }
}

export class UpdatePatientSuccess implements Action {
    readonly type = PatientActionTypes.UpdatePatientSuccess;

    constructor(public payload: { patient: Patient }) {
    }
}

export class DeletePatient implements Action {
    readonly type = PatientActionTypes.DeletePatient;

    constructor(public payload: { id: string }) {
    }
}

export class DeletePatientSuccess implements Action {
    readonly type = PatientActionTypes.DeletePatientSuccess;
    constructor(public payload: { id: string }) {
    }
}

export class PatientApiError implements Action {
    readonly type = PatientActionTypes.PatientApiError;

    constructor(public payload: { error: HttpErrorResponse }) {
    }
}

export type PatientActions =
    LoadPatients
    | LoadPatientsSuccess
    | AddPatient
    | AddPatientSuccess
    | UpdatePatient
    | UpdatePatientSuccess
    | DeletePatient
    | DeletePatientSuccess
    | PatientApiError;
