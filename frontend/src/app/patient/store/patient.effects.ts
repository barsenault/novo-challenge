import {Injectable} from '@angular/core';
import {Actions, Effect, ofType} from '@ngrx/effects';
import {PatientService} from '../services/patient.service';
import {ErrorHandlerService} from '../../core/services/error-handler.service';
import {
    AddPatient,
    AddPatientSuccess,
    DeletePatient,
    DeletePatientSuccess,
    LoadPatientsSuccess,
    PatientActionTypes,
    PatientApiError, UpdatePatient, UpdatePatientSuccess
} from './patient.actions';
import {catchError, concatMap, map, mergeMap, tap} from 'rxjs/operators';
import {HttpErrorResponse} from '@angular/common/http';
import {of} from 'rxjs';
import {PatientFormService} from '../services/patient-form.service';
import {NotificationService} from '../../core/services/notification.service';
import {Patient} from './patient.model';

@Injectable()
export class PatientEffects {
    constructor(private actions$: Actions,
                private patientService: PatientService,
                private errorHandler: ErrorHandlerService,
                private formService: PatientFormService,
                private notificationService: NotificationService) { }

    @Effect()
    loadPatient$ = this.actions$.pipe(
        ofType(PatientActionTypes.LoadPatients),
        mergeMap(() => this.patientService.getAll().pipe(
            map((patients) => {
                return new LoadPatientsSuccess({patients: patients});
            }),
            catchError((err: HttpErrorResponse) => of(new PatientApiError(err)))
        ))
    );

    @Effect()
    addPatient$ = this.actions$.pipe(
        ofType<AddPatient>(PatientActionTypes.AddPatient),
        concatMap(action =>
            this.patientService.addOne(action.payload.patient).pipe(
                map(value => new AddPatientSuccess({patient: value})),
                catchError((err: HttpErrorResponse) => of(new PatientApiError(err) ))
            )
        )

    );

    @Effect({dispatch: false})
    addPatientSuccess$ = this.actions$.pipe(
        ofType<AddPatientSuccess>(PatientActionTypes.AddPatientSuccess),
        map(action => action.payload.patient),
        tap((patient: Patient) => {
            this.formService.reset();
            this.notificationService.info('Patient added successfully.');
        })
    );

    @Effect()
    deletePatient = this.actions$.pipe(
        ofType<DeletePatient>(PatientActionTypes.DeletePatient),
        concatMap(action =>
            this.patientService.delete(action.payload.id).pipe(
                map(value => new DeletePatientSuccess({id: action.payload.id})),
                catchError((err: HttpErrorResponse) => of(new PatientApiError(err) ))
            )
        )
    );

    @Effect({dispatch: false})
    deletePatientSuccess = this.actions$.pipe(
        ofType<DeletePatientSuccess>(PatientActionTypes.DeletePatientSuccess),
        tap(action => {
            this.notificationService.info('Patient deleted successfully.');
        })
    );

    @Effect()
    updatePatient = this.actions$.pipe(
        ofType<UpdatePatient>(PatientActionTypes.UpdatePatient),
        concatMap(action =>
            this.patientService.updateOne(action.payload.patient.id.toString(), action.payload.patient.changes).pipe(
                map(value => new UpdatePatientSuccess({patient: value})),
                catchError((err: HttpErrorResponse) => of(new PatientApiError(err) ))
            )
        )
    );

    @Effect({dispatch: false})
    updatePatientSuccess = this.actions$.pipe(
        ofType<UpdatePatientSuccess>(PatientActionTypes.UpdatePatientSuccess),
        map(action => action.payload.patient),
        tap(patient => {
            this.notificationService.info('Patient updated successfully.');
        })
    );

    @Effect({dispatch: false})
    patientApiError$ = this.actions$.pipe(
        ofType(PatientActionTypes.PatientApiError),
        tap(error => {
            this.errorHandler.handleError(error);
        })
    );
}
