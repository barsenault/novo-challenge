import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Patient } from './patient.model';
import { PatientActions, PatientActionTypes } from './patient.actions';
import {createFeatureSelector, createSelector} from '@ngrx/store';

export interface State extends EntityState<Patient> {
  // additional entities state properties
}

export const adapter: EntityAdapter<Patient> = createEntityAdapter<Patient>();

export const initialState: State = adapter.getInitialState({
  // additional entity state properties
});

export function reducer(
  state = initialState,
  action: PatientActions
): State {
  switch (action.type) {
    case PatientActionTypes.AddPatientSuccess: {
      return adapter.addOne(action.payload.patient, state);
    }

    case PatientActionTypes.UpdatePatientSuccess: {
      return adapter.updateOne({id: action.payload.patient.id, changes: action.payload.patient}, state);
    }

    case PatientActionTypes.DeletePatientSuccess: {
      return adapter.removeOne(action.payload.id, state);
    }

    case PatientActionTypes.LoadPatientsSuccess: {
      return adapter.addAll(action.payload.patients, state);
    }

    default: {
      return state;
    }
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();

export const selectPatientState = createFeatureSelector<State>('patient');
export const selectAllPatients = createSelector(selectPatientState, selectAll);

