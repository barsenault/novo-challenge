import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {StoreModule} from '@ngrx/store';
import * as patientReducer from '../patient/store/patient.reducer';
import {EffectsModule} from '@ngrx/effects';
import {PatientEffects} from './store/patient.effects';
import { PatientInfoComponent } from './components/patient-info/patient-info.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MaterialModule} from '../material/material.module';
import { PatientFormComponent } from './components/patient-form/patient-form.component';
import { PatientAddComponent } from './components/patient-add/patient-add.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PatientFormService} from './services/patient-form.service';
import { PatientEditComponent } from './components/patient-edit/patient-edit.component';

@NgModule({
  declarations: [PatientInfoComponent, PatientFormComponent, PatientAddComponent, PatientEditComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature('patient', patientReducer.reducer),
    EffectsModule.forFeature([PatientEffects]),
      MaterialModule,
      FlexLayoutModule,
      FormsModule,
      ReactiveFormsModule,
  ],
  providers: [PatientFormService],
  exports: [PatientInfoComponent, PatientFormComponent],
  // The components used with Angular Material Dialogs need to be added to entry components
  entryComponents: [PatientAddComponent, PatientEditComponent]
})
export class PatientModule { }
