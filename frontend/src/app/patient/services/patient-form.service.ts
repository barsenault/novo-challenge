import {Injectable} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {Patient} from '../store/patient.model';

export interface Gender {
    value: string;
    label: string;
}

@Injectable()
export class PatientFormService {

    static GENDER_OPTIONS: Gender[] = [
        {
            value: 'male',
            label: 'Male'
        },
        {
            value: 'female',
            label: 'Female'
        }
    ];

    constructor(private formBuilder: FormBuilder) {

    }

    private _formGroup = this.formBuilder.group(
        {
            id: [''],
            firstname: ['', Validators.required],
            lastname: [''],
            email: ['', [Validators.email, Validators.required]],
            age: ['', [Validators.required, Validators.min(0), Validators.max(140)]],
            sex: ['', Validators.required],
            created_at: [''],
            updated_at: ['']
        }
    );

    get formGroup() {
        return this._formGroup;
    }

    get valid() {
        return this._formGroup.valid;
    }

    get value(): Patient {
        return this._formGroup.value;
    }

    reset() {
        this._formGroup.reset();
    }
}
