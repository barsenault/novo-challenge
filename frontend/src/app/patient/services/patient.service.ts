import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {Patient} from '../store/patient.model';

// Todo: extract endpoint host into a settings service
export const ENDPOINT = 'http://novo-backend.duckdns.org/api/patient';

@Injectable({
    providedIn: 'root'
})
export class PatientService {

    constructor(private http: HttpClient) {}

    getAll(): Observable<Patient[]> {
        return this.http.get<Patient[]>(ENDPOINT);
    }

    addOne(patient: Patient) {
        return this.http.post<Patient>(ENDPOINT, patient);
    }

    delete(id: string) {
        return this.http.delete(ENDPOINT + '/' + id);
    }

    updateOne(id: string, changes: Partial<Patient>) {
        return this.http.put<Patient>(ENDPOINT + '/' + id, changes);
    }
}
