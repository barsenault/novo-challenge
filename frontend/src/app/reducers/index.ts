import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../environments/environment';

/*
  This is the global app reducer.
  Feature specific reducers (ex. Patient or VitalSign) are located in their respective directories.
*/

export interface State { }

export const reducers: ActionReducerMap<State> = {

};

export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];




