import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfirmDeleteComponent } from './components/confirm-delete/confirm-delete.component';
import {MaterialModule} from '../material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';

@NgModule({
  declarations: [ConfirmDeleteComponent],
  imports: [
    CommonModule,
      MaterialModule,
      FlexLayoutModule
  ],
  entryComponents: [ConfirmDeleteComponent],
  exports: [ConfirmDeleteComponent]
})
export class SharedModule { }
