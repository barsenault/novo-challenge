import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Patient} from '../../../patient/store/patient.model';
import {PatientAddComponent} from '../../../patient/components/patient-add/patient-add.component';
import {AddPatient, DeletePatient, UpdatePatient} from '../../../patient/store/patient.actions';
import {ConfirmDeleteComponent} from '../../../shared/components/confirm-delete/confirm-delete.component';
import {PatientEditComponent} from '../../../patient/components/patient-edit/patient-edit.component';
import {MatDialog} from '@angular/material';

@Component({
    selector: 'app-patient-summary-item',
    templateUrl: './patient-summary-item.component.html',
    styleUrls: ['./patient-summary-item.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PatientSummaryItemComponent implements OnInit {
    @Output() patientDelete = new EventEmitter();
    @Output() patientEdit = new EventEmitter();

    @Input() summary;

    patientOpenState = true;
    statOpenState = false;

    constructor(private dialog: MatDialog) {
    }

    get fullname() {
        return this.summary.patient.firstname + ' ' + this.summary.patient.lastname;
    }

    ngOnInit() {

    }

    edit(patient: Patient) {
        const dialogRef = this.dialog.open(PatientEditComponent, {data: patient});
        dialogRef.afterClosed().subscribe((result: Patient) => {
            if (result) {
                this.patientEdit.emit(result);
            }
        });
    }

    delete(patient: Patient) {
        const data = {title: 'Delete ' + patient.firstname + ' ' + patient.lastname + ' ?'};
        const dialogRef = this.dialog.open(ConfirmDeleteComponent, {data: data});
        dialogRef.afterClosed().subscribe((result: boolean) => {
            if (result) {
                this.patientDelete.emit(patient);
            }
        });
    }
}
