import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientSummaryItemComponent } from './patient-summary-item.component';

describe('PatientSummaryItemComponent', () => {
  let component: PatientSummaryItemComponent;
  let fixture: ComponentFixture<PatientSummaryItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientSummaryItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientSummaryItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
