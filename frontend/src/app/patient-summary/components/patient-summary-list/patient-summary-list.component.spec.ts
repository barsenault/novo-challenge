import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientSummaryListComponent } from './patient-summary-list.component';

describe('PatientSummaryListComponent', () => {
  let component: PatientSummaryListComponent;
  let fixture: ComponentFixture<PatientSummaryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientSummaryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientSummaryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
