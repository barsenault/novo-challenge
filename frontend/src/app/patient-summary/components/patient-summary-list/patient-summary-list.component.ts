import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Observable} from 'rxjs';
import {PatientSummary} from '../../models/patient-summary';
import {Patient} from '../../../patient/store/patient.model';

@Component({
    selector: 'app-patient-summary-list',
    templateUrl: './patient-summary-list.component.html',
    styleUrls: ['./patient-summary-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class PatientSummaryListComponent implements OnInit {

    @Input() summaries: Observable<PatientSummary[]>;

    @Output() patientEdit = new EventEmitter();
    @Output() patientDelete = new EventEmitter();

    constructor() {
    }

    ngOnInit() {
    }

    trackByFn(index, item) {
        return item.id; // or item.id
    }

    onPatientEdit(patient: Patient) {
        this.patientEdit.emit(patient);
    }

    onPatientDelete(patient: Patient) {
        this.patientDelete.emit(patient);
    }
}
