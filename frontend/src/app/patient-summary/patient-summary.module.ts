import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PatientSummaryItemComponent} from './components/patient-summary-item/patient-summary-item.component';
import {PatientSummaryListComponent} from './components/patient-summary-list/patient-summary-list.component';
import {PatientSummaryComponent} from './containers/patient-summary/patient-summary.component';
import {MaterialModule} from '../material/material.module';
import {FlexLayoutModule} from '@angular/flex-layout';
import {VitalSignModule} from '../vitalsign/vital-sign.module';
import {PatientModule} from '../patient/patient.module';
import {ChartsModule} from 'ng2-charts';

@NgModule({
    declarations: [PatientSummaryItemComponent, PatientSummaryListComponent, PatientSummaryComponent],
    imports: [
        CommonModule,
        MaterialModule,
        FlexLayoutModule,
        PatientModule,
        VitalSignModule
    ],
    exports: [PatientSummaryComponent]
})
export class PatientSummaryModule {
}
