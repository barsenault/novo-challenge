import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {PatientSummaryPresenterService} from '../../presenter/patient-summary-presenter.service';
import {Patient} from '../../../patient/store/patient.model';
import {MatDialog} from '@angular/material';
import {PatientAddComponent} from '../../../patient/components/patient-add/patient-add.component';
import {AddPatient} from '../../../patient/store/patient.actions';

@Component({
    selector: 'app-patient-summary',
    templateUrl: './patient-summary.component.html',
    styleUrls: ['./patient-summary.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PatientSummaryComponent implements OnInit, OnDestroy {

    patientSummaries$ = this.presenter.getPatientSummary();

    constructor(private presenter: PatientSummaryPresenterService, private dialog: MatDialog) {
    }

    ngOnInit() {
        this.presenter.startPolling();
    }

    ngOnDestroy(): void {
        this.presenter.stopPolling();
    }

    onPatientDelete(patient: Patient) {
        this.presenter.deletePatient(patient);
    }

    onPatientEdit(patient: Patient) {
        this.presenter.editPatient(patient);
    }

    onAddClicked() {
        const dialogRef = this.dialog.open(PatientAddComponent, {});
        dialogRef.afterClosed().subscribe((result: Patient) => {
            if (result) {
                this.presenter.addPatient(result);
            }
        });
    }
}
