import {VitalSign} from '../../vitalsign/store/vital-sign.model';
import {Patient} from '../../patient/store/patient.model';
import {VitalSignStat} from '../../vitalsign/store/vital-sign-stat.model';

//export type PatientSummary = Patient | VitalSign | VitalSignStat;

export interface PatientSummary {
    patient: Patient;
    vitalSign: VitalSign;
    vitalSignStat: VitalSignStat;
}
