import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {combineLatest, Observable} from 'rxjs';
import {Patient} from '../../patient/store/patient.model';
import {VitalSign} from '../../vitalsign/store/vital-sign.model';
import {map} from 'rxjs/operators';
import {AddPatient, DeletePatient, LoadPatients, UpdatePatient} from '../../patient/store/patient.actions';
import {PollVitalSigns, StopVitalSignsPolling} from '../../vitalsign/store/vital-sign.actions';
import {PatientSummary} from '../models/patient-summary';
import {selectAllVitalSigns} from '../../vitalsign/store/vital-sign.reducer';
import {selectAllPatients} from '../../patient/store/patient.reducer';
import {VitalSignStat} from '../../vitalsign/store/vital-sign-stat.model';
import {selectAllVitalSignStats} from '../../vitalsign/store/vital-sign-stat.reducer';
import {PollVitalSignStats, StopVitalSignStatsPolling} from '../../vitalsign/store/vital-sign-stat.actions';

@Injectable({
    providedIn: 'root'
})
export class PatientSummaryPresenterService {

    patients$: Observable<Patient[]>;
    vitalSigns$: Observable<VitalSign[]>;
    vitalSignStats$: Observable<VitalSignStat[]>;

    patientSummary$: Observable<PatientSummary[]>;

    constructor(private store: Store<any>) {
        this.patients$ = this.store.select(selectAllPatients);
        this.vitalSigns$ = this.store.select(selectAllVitalSigns);
        this.vitalSignStats$ = this.store.select(selectAllVitalSignStats);

        /*
        Combine the Patients[] stream with our polled VitalSigns[] and VitalSignsStats[] streams,
         by their ids, into a PatientSummary[] stream.
         */
        this.patientSummary$ =
            combineLatest<Patient[], VitalSign[], VitalSignStat[]>(this.patients$, this.vitalSigns$, this.vitalSignStats$)
                .pipe(
                    map(([patients, vitalSigns, vitalSignStats]) =>
                        patients.map(patient => {
                            const vitalSign = vitalSigns.find(s => patient.id === s.patient_id);
                            const vitalSignStat = vitalSignStats.find(s => patient.id === s.patient_id);
                            return Object.assign({patient: patient, vitalSign: vitalSign, vitalSignStat: vitalSignStat});
                        })
                    )
                );
    }

    getPatientSummary(): Observable<PatientSummary[]> {
        return this.patientSummary$;
    }

    startPolling() {
        this.store.dispatch(new LoadPatients());
        this.store.dispatch(new PollVitalSigns());
        this.store.dispatch(new PollVitalSignStats());
    }

    stopPolling() {
        this.store.dispatch(new StopVitalSignsPolling());
        this.store.dispatch(new StopVitalSignStatsPolling());
    }

    deletePatient(patient: Patient) {
        this.store.dispatch(new DeletePatient({id: patient.id}));
    }

    editPatient(patient: Patient) {
        this.store.dispatch(new UpdatePatient({patient: {id: patient.id, changes: patient}}));
    }

    addPatient(patient: Patient) {
        this.store.dispatch(new AddPatient({patient: patient}));
    }
}
