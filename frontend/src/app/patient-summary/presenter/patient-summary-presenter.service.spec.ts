import { TestBed } from '@angular/core/testing';

import { PatientSummaryPresenterService } from './patient-summary-presenter.service';

describe('PatientSummaryPresenterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PatientSummaryPresenterService = TestBed.get(PatientSummaryPresenterService);
    expect(service).toBeTruthy();
  });
});
